<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage ABC
 * @since ABC 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.5.7/galleria.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/smoothscroll.js"></script>
</head>

<body>

<div class="navbar">
    <div class="navbar-header">
        <a class="navbar-brand" href="/"> ABC </a>
    </div>

    <div class="container">
    <ul class="navbar-right navbar-nav">
        <?php
        /* Loop through pages and echo content to div with title for class */ 
        $pages = get_pages(); 
        foreach ($pages as $page_data) { 
        $page_title = $page_data->post_title;
        ?>
            <li><a class="<?php if ($page_title == 'Home')  echo 'Active'  ?>" href="#<?php echo $page_title ?>"><?php echo $page_title ?></a></li>
        <?php } ?>
    </ul>
        </div>

</div>