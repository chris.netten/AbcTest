<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage ABC
 * @since ABC 1.0
 */
?>

<div class="footer">
    <div class="inner-foot">
        
    <div class="container">
    <ul id="foot-nav">
        <?php
        /* Loop through pages and echo content to div with title for class */ 
        $pages = get_pages(); 
        foreach ($pages as $page_data) { 
        $page_title = $page_data->post_title;
        ?>
            <li><a href="#<?php echo $page_title ?>"><?php echo $page_title ?></a></li>
        <?php } ?>
    </ul>

        <ul id="foot-nav-right">
            <li id="grey-block"></li>
            <li id="grey-block"></li>
            <li id="grey-block"></li>
        </ul>
        </div>

        

    </div>
</div>
<script>
	(function() { 
            Galleria.loadTheme('https://cdnjs.cloudflare.com/ajax/libs/galleria/1.5.7/themes/classic/galleria.classic.min.js');
            Galleria.run('.galleria');
        }());
        </script>
<?php wp_footer(); ?>
