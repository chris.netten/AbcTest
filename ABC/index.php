<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage ABC
 * @since ABC 1.0
 */

 get_header(); ?>



<?php
/* Loop through pages and echo content to div with title for id */ 
$pages = get_pages(); 
foreach ($pages as $page_data) { 
$page_title = $page_data->post_title;
?>
    <div id="<?php echo $page_title?>">
        <?php
        $content = apply_filters(‘the_content’, $page_data->post_content); 
        echo  $content;
        ?>
    </div>
<?php
}
?>


<?php get_footer(); ?>